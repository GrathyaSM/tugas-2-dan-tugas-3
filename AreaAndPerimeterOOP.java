import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AreaAndPerimeterOOP{
	public static void menu (){
		System.out.println("\nMenu");
		System.out.println("1. Two Dimensions");
		System.out.println("2. Three Dimensions");
		System.out.println("0. Exit");
	}
	
	public static void main (String [] args){
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String input = null;
		int choice = 0;
		do{
			menu();
			System.out.print("\nSubmit your Choice : ");
			try{
				input = bufferedReader.readLine();
				try{
					choice = Integer.parseInt (input);
					if (choice > 0 && choice == 1){
						twoD.twoDimensions();
						System.out.println("Submit your Choice");
						try{
							input = bufferedReader.readLine();
							try{ 
								choice = Integer.parseInt (input);
								if (choice >0 && choice == 1){
									square.Square();
								}else if (choice > 0 && choice == 2){
									rectangle.Rectangle();
								}else if (choice > 0 && choice == 3){
									triangle.Triangle();
								}else if (choice > 0 && choice == 4){
									parallelogram.Parallelogram ();
								}else if (choice > 0 && choice == 5){
									trapezoid.Trapezoid();
								}else if (choice > 0 && choice == 6){
									circle.Circle();
								}else if (choice > 0 && choice == 7){
									kite.Kite();
								}else if (choice > 0 && choice == 8){
									rhombus.Rhombus();
								}else if (choice > 0 && choice == 9){
									ellips.Ellipse();
								}else if (choice > 0 && choice == 10){
									hexagon.Hexagon(); 
								}else if (choice > 0 && choice == 11){
									octagon.Octagon();
								}else if (choice > 0 && choice == 0){
									System.out.println("Thank You :)");
								}else {
									System.out.println("Your input is wrong, please reinput your data, Thank you");
								}
							}catch (NumberFormatException e){
								System.out.println("Your input is wrong");
								}
						}catch (IOException error){
							System.out.println("Error input"+error.getMessage());
						}
					}else if (choice > 0 && choice == 2){
						threeD.threeDimensions();
						System.out.println("Submit your choice : ");
						try{
							choice = Integer.parseInt(input);
							if (choice > 0 && choice == 1){
								cube.Cube();
							}else if (choice > 0 && choice == 2){
								cuboid.Cuboid();
							}else if (choice > 0 && choice == 3){
								cylinder.Cylinder();
							}else if (choice > 0 && choice == 4){
								cone.Cone();
							}else if (choice > 0 && choice == 5){
								prism.Prism();
							}else if ( choice > 0 && choice == 6){
								pyramid.Pyramid();
							}else if ( choice > 0 && choice == 7){
								sphere.Sphere();
							}else if (choice > 0 && choice == 0){
								System.out.println("Thank you");
							}else{
								System.out.println("Your input is wrong, please reinput your data, Thank you");
							}
						}catch(NumberFormatException e){
							System.out.println("Your input is wrong");
						}
					}else{
						System.out.println("Thank you");
					}
				
				}catch (NumberFormatException e){
					System.out.println("your Input is wrong");
				}
			}catch (IOException error){
						System.out.println("Error Input"+error.getMessage());
					}
		}while (choice > 0);
	}
}

class twoD extends AreaAndPerimeterOOP {
	public static void twoDimensions(){
		System.out.println("\nMenu of Two Dimensions : ");
		System.out.println("1. Square");
		System.out.println("2. Rectangle");
		System.out.println("3. Triangle");
		System.out.println("4. Parallelogram");
		System.out.println("5. Trapezoid");
		System.out.println("6. Circle");
		System.out.println("7. Kite");
		System.out.println("8. Rhombus");
		System.out.println("9. Ellipse");
		System.out.println("10. Hexagon");
		System.out.println("11. Octagon");
		System.out.println("0. Exit");
	}
}

class threeD extends AreaAndPerimeterOOP{
	public static void threeDimensions(){
		System.out.println("\nMenu of Three Dimensions : ");
		System.out.println("1. Cube");
		System.out.println("2. Cuboid");
		System.out.println("3. Cylinder");
		System.out.println("4. Cone");
		System.out.println("5. Prism");
		System.out.println("6. Pyramid");
		System.out.println("7. Sphere");
		System.out.println("0. Exit");
	}
}
	
class square extends AreaAndPerimeterOOP {
	public static void Square (){
		System.out.println("\nArea and Perimeter Of a Square");
		System.out.print("Input side : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String lenght = null;
		try{
			lenght = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error! Input data Correctly"+error.getMessage());
		}
		
		try{
			float side = Float.parseFloat(lenght);
			float perimeter = 4*side;
			float area = side*side;
			System.out.println("Perimeter of a square is "+perimeter);
			System.out.println("Area of a square is "+area+"\n");
		}catch(NumberFormatException e){
			System.out.println("Your submit is wrong");
		}
	}
}	

class rectangle extends AreaAndPerimeterOOP {
	public static void Rectangle (){
		System.out.println("\n Area and Perimeter of a Rectangle");
		System.out.print("Input lenght : ");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String lenght = null;
		try{
			lenght = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data Correctly"+error.getMessage());
		}
		
		System.out.print("Input width : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String width = null;
		try{
			width = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		try{
			float Lenght = Float.parseFloat(lenght);
			float Width = Float.parseFloat(width);
			float perimeter = 2*(Lenght+Width);
			float area = Lenght*Width;
			System.out.println("Area of a Rectangle : " + perimeter);
			System.out.println("Area of a Rectangle : " + area+"\n");
		}catch(NumberFormatException e){
			System.out.println("Your submit is wrong");
		}
	}
}

class triangle extends AreaAndPerimeterOOP{
	public static void Triangle(){
		System.out.println("\nArea Of a Triangle");
		System.out.println("1. Triangle");
		System.out.println("2. Equilateral Triangle");
		System.out.println("3. Isoscles Triangle");
		System.out.println("\nTriangle");
		System.out.print("Input Base :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String base = null;
		try{
			base = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input base correctly"+error.getMessage());
		}
		
		System.out.print("Input Height :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try{
			height = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input base correctly"+error.getMessage());	
		}
		
		System.out.print("Input Side 1 :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String side1 = null;
		try{
			side1 = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input side correctly"+error.getMessage());
		}
		
		System.out.print("Input Side 2 :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String side2 = null;
		try{
			side2 = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input side correctly"+error.getMessage());
		}
		
		System.out.print("Input Side 3 :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String side3 = null;
		try{
			side3 = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input side correctly"+error.getMessage());
		}
		
		System.out.println("\nEquilateral triangle");
		System.out.print("Input side : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String side = null;
		try{
			side = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input lenght correctly"+error.getMessage());
		}
		
		System.out.println("\nIsosceles triangle");
		System.out.print("Input side : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String isoscelesside = null;
		try{
			isoscelesside = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input lenght correctly"+error.getMessage());
		}
		
		System.out.print("Input base : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String isoscelesbase = null;
		try{
			isoscelesbase = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input lenght correctly"+error.getMessage());
		}
		
		System.out.print("Input height : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String isoscelesheight = null;
		try{
			isoscelesheight = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input lenght correctly"+error.getMessage());
		}
		
		try{
			float Base = Float.parseFloat(base);
			float Height = Float.parseFloat (height);
			float Side1 = Float.parseFloat (side1);
			float Side2 = Float.parseFloat(side2);
			float Side3 = Float.parseFloat(side3);
			float Side = Float.parseFloat (side);
			float IsoscelesSide = Float.parseFloat(isoscelesside);
			float IsoscelesBase = Float.parseFloat(isoscelesbase);
			float IsoscelesHeight = Float.parseFloat (isoscelesheight);
			double PerimeterTriangle = Side1 + Side2 + Side3;
			double Triangle = 0.5 * Base * Height;
			double EquilateralTriangle = 3 * Side;
			double areaEquilateralTriangle = 0.433 * Math.pow(Side, 2);
			double Isosceles = (2 * IsoscelesSide) + IsoscelesBase;
			double AreaIsosceles = (IsoscelesBase * IsoscelesHeight) / 2;
			System.out.print("\nPerimeter of a Triangle is "+PerimeterTriangle);
			System.out.print("Area of a Triangle is "+Triangle);
			System.out.print("\nPerimeter of a Equilateral Triangle is "+EquilateralTriangle);
			System.out.print("Area of a Equilateral Triangle is "+areaEquilateralTriangle);
			System.out.print("\nPerimeter of a Isoscles Triangle is "+Isosceles);
			System.out.print("Area of a Isosceles Triangle is "+AreaIsosceles);
		}catch(NumberFormatException e){
			System.out.println("Your Input is wrong");
		}
	}
}

class parallelogram extends AreaAndPerimeterOOP {
	public static void Parallelogram(){
		System.out.println("\nArea and Perimeter Of a Parallelogram");
		System.out.print("Input base :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String base = null;
		try{
			base = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data correctly"+error.getMessage());
		}
		
		System.out.print("Input side :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String side = null;
		try{
			side = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data correctly"+error.getMessage());
		}
		
		System.out.print("Input height :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try{
			height = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data correctly"+error.getMessage());
		}
		
		try{
			float Base = Float.parseFloat(base);
			float Height = Float.parseFloat (height);
			float Side = Float.parseFloat (side);
			float area = Base * Height;
			float perimeter = 2 * (Base + Side);
			System.out.print("Perimeter of a Parallelogram is "+perimeter);
			System.out.print("Area of a Parallelogram is "+area+"\n");
		}catch(NumberFormatException e){
			System.out.println("Your Submit is wrong");
		}
	}
}

class trapezoid extends AreaAndPerimeterOOP{
	public static void Trapezoid(){
		System.out.println("\nArea and Perimeter Of a Trapezoid");
		System.out.print("Input parallel side A :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String sideA = null;
		try{
			sideA = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input parallel side B :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String sideB = null;
		try{
			sideB = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input parallel base C :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String baseC = null;
		try{
			baseC = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input parallel base D :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String baseD = null;
		try{
			baseD = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input parallel height :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try{
			height = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		try{
			float side1 = Float.parseFloat(sideA);
			float side2 = Float.parseFloat(sideB);
			float Height = Float.parseFloat(height);
			float base1  = Float.parseFloat(baseC);
			float base2 = Float.parseFloat(baseD);
			float perimeter = base1 + base2 + side1 + side2;
			float area  = ((side1 + side2) * Height)/2;
			System.out.print("Perimeter of a Trapezoid is "+perimeter);
			System.out.print("Area of a Trapezoid is "+area+"\nArea");
		}catch(NumberFormatException e){
			System.out.println("Your input is wrong");
		}
	}
}

class circle extends AreaAndPerimeterOOP {
	public static void Circle(){
		System.out.println("\nArea and Perimeter Of a Circle");
		System.out.println("1. Diameter ");
		System.out.println("2. Radius ");
		System.out.println("0.Exit");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Submit your choice : ");
		int choose=0;
        String choice = null;
        try{
			choice = bufferedReader.readLine();
			try{
				choose = Integer.parseInt(choice);
        		if (choose>0 && choose == 1){
        			System.out.print("\nInput Diameter  :");
        			bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        	        String diameter = null;
        	        try {
        	            diameter = bufferedReader.readLine();
        	        }
        	        catch (IOException error) {
        	            System.out.println("Error ! Input length correctly " + error.getMessage());
        	        }
        	        try  {
        	            float Diameter = Float.parseFloat(diameter);
        	            double area = (3.14/4)*Math.pow(Diameter, 2);
        	            double perimeter = 2*3.14*Diameter;
        	            System.out.println("Perimeter of a Circle (using Diameter) is " + perimeter);
        	            System.out.println("Area of a Circle (using Diamater) is " + area + "\n");
        	        }
        	        catch(NumberFormatException e) {
        	            System.out.println("Your Input is wrong");
        	        }
				}else if(choose > 0 && choose == 2){
        			System.out.print("\nInput Radius  :");
        			bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        	        String radius = null;
        	        try {
        	            radius = bufferedReader.readLine();
        	        }
        	        catch (IOException error) {
        	            System.out.println("Error ! Input data correctly " + error.getMessage());
        	        }
        	        try  {
        	            float Radius = Float.parseFloat(radius);
        	            double perimeter = (2*3.14*Radius);
        	            double area = 3.14*Math.pow(Radius, 2);
        	            System.out.println("Perimeter of a Circle (using Radius) is " + perimeter);
        	            System.out.println("Area of a Circle (using Radius) is " + area +"\n");
        	        }
        	        catch(NumberFormatException e) {
        	            System.out.println("Your Input is wrong");
        	        }
				}else{
        			System.out.println("Thank you");
        		}
			}catch(NumberFormatException e) {
        	            System.out.println("Your Input is wrong");
        	        }
		}catch (IOException error){
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
	}		
}

class kite extends AreaAndPerimeterOOP {
	public static void Kite () {
		System.out.println("\nArea and Perimeter Of a Kite");
		System.out.print("Input length of Side A :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String sideA = null;
		try{
			sideA = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error ! Input length correctly " + error.getMessage());
		}
		
		System.out.print("Input length of Side B :");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String sideB = null;
		try{
			sideB = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input length correctly " + error.getMessage());
		}
		
		System.out.print("Input length of Diagonal 1 :");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String diagonal1 = null;
		try{
			diagonal1 = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input length correctly " + error.getMessage());
		}
		
		System.out.print("Input length of Diagonal 2 :");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String diagonal2 = null;
		try{
			diagonal2 = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println("Error ! Input length correctly " + error.getMessage());
		}
		
		try{
			float D1 = Float.parseFloat(diagonal1);
			float D2 = Float.parseFloat(diagonal2);
			float SideA = Float.parseFloat(sideA);
			float SideB = Float.parseFloat(sideB);
			float perimeter = (D1 * D2)/2;
			float area = (SideA + SideB)*2;
			System.out.println("Perimeter of a Kite is " +perimeter);
            System.out.println("Area of a Kite is " + area+"\n");
		}catch(NumberFormatException e) {
        	System.out.println("Your Input is wrong");
        }
	}
}

class rhombus extends AreaAndPerimeterOOP{
	public static void Rhombus (){
		System.out.println("\nArea and Perimeter Of a Rhombus");
		System.out.print("Input length : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String length = null;
		try {
        	length = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input length correctly " + error.getMessage());
        }
		
		System.out.print("Input height :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String height = null;
		try {
			height = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input height correctly " + error.getMessage());
		}
		
		System.out.print("Input side :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String side = null;
		try {
			side = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input height correctly " + error.getMessage());
		}
		
		try  {
			float Length = Float.parseFloat(length);
			float Height = Float.parseFloat(height);
			float Side= Float.parseFloat(side);
			float area = Length * Height;
			float perimeter = 4 * Side;
			System.out.println("Perimeter of a Rhombus is " + perimeter);
			System.out.println("Area of a Rhombus is " + area+"\n");
		}catch(NumberFormatException e) {
			System.out.println("Your Input is wrong");
		}
	}
}

class ellips extends AreaAndPerimeterOOP{
	public static void Ellipse(){
		System.out.println("\nArea Of a Ellipse");
		System.out.print("Input semiminor axis ( r1 ) : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String r1 = null;
		try {
			r1 = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input semimajor axis ( r2 ) : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String r2 = null;
		try {
			r2 = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
			
		try  {
			float Radius1 = Float.parseFloat(r1);
			float Radius2 = Float.parseFloat(r2);
			double area = 3.14*(Radius1*Radius2);
			System.out.println("Area of a Ellipse is " + area +"\n");
		}catch(NumberFormatException e) {
			System.out.println("Your Input is wrong");
		}
	}
}

class hexagon extends AreaAndPerimeterOOP{
	public static void Hexagon(){
		System.out.println("\nArea and Perimeter Of a Hexagon");
		System.out.print("Input Radius : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String radius = null;
		try {
			radius = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
			
		try  {
			float Radius = Float.parseFloat(radius);
			double perimeter = 6*Radius;
			double area = ((3*Math.sqrt(3)/2)*Math.pow(Radius, 2));
			System.out.println("Perimeter of a Hexagon is " + perimeter);
			System.out.println("Area of a Hexagon is " + area +"\n");
		}catch(NumberFormatException e) {
			System.out.println("Your Input is wrong");
		}
	}
}

class octagon extends AreaAndPerimeterOOP{
	public static void Octagon(){
		System.out.println("\nArea and Perimeter Of a Octagon");
		System.out.print("Input Radius : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String radius = null;
		try {
			radius = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
			
		try  {
			float Radius = Float.parseFloat(radius);
			double perimeter = 8*Radius;
			double area = ((2*(1+Math.sqrt(2))*Math.pow(Radius, 2)));
			System.out.println("Perimeter of a Octagon is " + perimeter);
			System.out.println("Area of a Octagon is " + area +"\n");
		}catch(NumberFormatException e) {
			System.out.println("Your Input is wrong");
		}
	}
}
	
class cube extends AreaAndPerimeterOOP{
	public static void Cube(){
		System.out.println("\nVolume and Surface area Of a Cube");
		System.out.print("Input side : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String side = null;
		try{
			side = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input Data Correctly"+error.getMessage());
		}
		try{
			float Side = Float.parseFloat(side);
			float Volume = Side * Side * Side;
			float area = 6 * ( Side * Side);
			System.out.println("Volume of a Cube is "+Volume);
			System.out.println("Surface area of a cube is "+area+"\n");
			
		}catch(NumberFormatException e){
			System.out.println("Your submit is wrong");
		}
	}
}	
	
class cuboid extends AreaAndPerimeterOOP{
	public static void Cuboid(){
		System.out.println("\nVolume and Surface area Of a Cuboid");
		System.out.print("Input length : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String length = null;
		try{
			length = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input Data Correctly"+error.getMessage());
		}
			
		System.out.print("Input width : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String width = null;
	    try {
	        width = bufferedReader.readLine();
	    }catch (IOException error) {
	        System.out.println("Error ! Input data correctly " + error.getMessage());
	    }
	    
		System.out.print("Input height : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try {
			height = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		try{
			float Length = Float.parseFloat(length);
			float Width = Float.parseFloat(width);
			float Height = Float.parseFloat(height);
			float Volume = Length*Width*Height;
			float SurfaceArea = (2*Length*Height)+(2*Length*Width)+(2*Width*Height);
			System.out.println("Volume Surface area Of a Cuboid"+Volume);
			System.out.println("Surface area Of a Cuboid is "+SurfaceArea+"\n");
		}catch(NumberFormatException e){
			System.out.println("Your submit is wrong");
		}
	}
}
	
class cylinder extends AreaAndPerimeterOOP{
	public static void Cylinder (){
		System.out.println("\nVolume and Surface area Of a Cylinder");
		System.out.print("\nInput Radius  :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String radius = null;
		try {
			radius = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input height : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try {
			height = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
        
		try  {
            float Radius = Float.parseFloat(radius);
            float Height = Float.parseFloat(height);
            double Volume  =(3.14*Math.pow(Radius, 2)*Height);
            double SurfaceArea = (2*3.14*Math.pow(Radius, 2))+(2*3.14*Height);
			System.out.println("Volume Surface area Of a Cylinder is "+Volume);
			System.out.println("Surface area Of a Cylinder is "+SurfaceArea+"\n");
        }catch(NumberFormatException e) {
            System.out.println("Your Input is wrong");
		}
	}
}

class cone extends AreaAndPerimeterOOP{
	public static void Cone (){
		System.out.println("\nVolume and Surface area Of a Cone");
		
		System.out.print("Input height : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try {
		   height = bufferedReader.readLine();
		}catch (IOException error) {
		   System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		System.out.print("Input Radius : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String radius = null;
		try {
			radius = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}  
		
		try  {
			float Radius =Float.parseFloat(radius);
			float Height = Float.parseFloat(height);
			double Volume = 3.14*Math.pow(Radius, 2)*(Height/3);
			double SurfaceArea = 3.14*Radius*(Radius+Math.sqrt((Math.pow(Height, 2)+(Math.pow(Radius, 2)))));
			System.out.println("Volume Surface area Of a Cone is "+Volume);
			System.out.println("Surface area Of a Cone is "+SurfaceArea+"\n");
		}catch(NumberFormatException e) {
			System.out.println("Your Input is wrong");
		}
	}
}	

class pyramid extends AreaAndPerimeterOOP{
	public static void Pyramid (){
		System.out.println("\nVolume and Surface area Of a Pyramid");
		System.out.print("Input length : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String length = null;
		try{
			length = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input Data Correctly"+error.getMessage());
		}
			
		System.out.print("Input width : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	    String width = null;
		try {
			width = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
	    
		System.out.print("Input height : ");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String height = null;
		try {
			height = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		}
		
		try{
			float Length = Float.parseFloat(length);
			float Width = Float.parseFloat(width);
			float Height = Float.parseFloat(height);
			float Volume = (Length*Width*Height)/3;
			float SurfaceArea = (2*Length*Height)+(2*Length*Width)+(2*Width*Height);
			System.out.println("Volume Surface area Of a Pyramid "+Volume);
			System.out.println("Surface area Of a Pyramid is "+SurfaceArea+"\n");
		}catch(NumberFormatException e){
			System.out.println("Your submit is wrong");
		}
	}
}

class sphere extends AreaAndPerimeterOOP{
	public static void Sphere (){
		System.out.println("\nVolume and Surface area Of a Sphere");
		System.out.print("Input Radius : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String radius = null;
		try {
			radius = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input data correctly " + error.getMessage());
		} 
			
		try  {
			float Radius = Float.parseFloat(radius);
			double Volume = (4/3)*3.14*Math.pow(Radius, 3);
			double SurfaceArea = 4*3.14*Math.pow(Radius, 2);
			System.out.println("Volume Surface area Of a Sphere is "+Volume);
			System.out.println("Surface area Of a Sphere is "+SurfaceArea+"\n");
		}catch(NumberFormatException e) {
			System.out.println("Your Input is wrong");
		}
	}
}

class prism extends AreaAndPerimeterOOP{
	public static void Prism (){
		System.out.println("\nVolume and Surface area Of a Prism");
		System.out.print("Input height :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String height = null;
        try {
        	height = bufferedReader.readLine();
        }catch (IOException error) {
        	System.out.println("Error ! Input height correctly " + error.getMessage());
        }
        
		System.out.print("input data for base of triangle");
        System.out.print("Input base :");
    	bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String base = null;
		try {
			base = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input height correctly " + error.getMessage());
		}
        
		System.out.print("Input height :");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String baseHeight = null;
		try {
			base = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error ! Input height correctly " + error.getMessage());
		}
		
		try  {
        	float altitude = Float.parseFloat(height);
        	float Base =  Float.parseFloat(base);
        	float Baseheight = Float.parseFloat(baseHeight);
        	float TriangleBase = (1/2*Base*Baseheight);
        	float volume = TriangleBase*altitude;
        	System.out.println("Surface area of a Prism is " +TriangleBase);
            System.out.println("Volume of a Prism is " +volume+"\n");
        }catch(NumberFormatException e) {
        	System.out.println("Your Input is wrong");
        }
	}
}