import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class MatrixOOP {
	public static void main (String[] args) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        int choice = 0;
        
        do{
        	matrixmenu();
        	System.out.print("\nSubmit your choice : ");
        	try{
        		input = bufferedReader.readLine();
        		try{
        			choice = Integer.parseInt(input);
        			if (choice > 0 && choice == 1){
        				Addition.addition();
        			}
        			else if  (choice > 0 && choice == 2){
        				Reduction.reduction();
        			}
        			else if  (choice > 0 && choice == 3){
        				Multiplication.multiplication();
        			}
        			else if (choice > 0 && choice == 4){
        				Determinant.determinant();
        			}
        			else if (choice == 0){
        				System.out.println("Thank you :)");
        			}
        			else{
        				 System.out.println("Your input is wrong, please reinput your data, Thank you :)");
        			}
        	}
        		catch(NumberFormatException e) {
		         System.out.println("Your input is wrong, please reinput your data, Thank you :)");
		         }
        	}
        	catch (IOException error) {
        		System.out.println("Error Input " + error.getMessage());
            }
        }while(choice > 0 );
	}
	
	public static void matrixmenu (){
		System.out.println("\nMenu Of Matrix");
		System.out.println("1. Addition matrix ");
		System.out.println("2. Reduction matrix ");
		System.out.println("3. Multiplication matrix");
		System.out.println("4. Determinant matrix");
		System.out.println("0. Exit");
		
	}
}

class Addition extends MatrixOOP{
	public static void addition(){
		System.out.println("\nAddition Matrix");
		System.out.println("Input Data for Matriks A ");
		System.out.print("Ordo :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String ordo = null;
			try{
				ordo = bufferedReader.readLine();
			}
			catch(IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}
			
			
		int Ordo = Integer.parseInt(ordo);
		int[][]matrixA = new int [Ordo][Ordo];
		
		System.out.println("\nData for Matriks A");
			for (int field=0; field < matrixA.length; field++){
				for (int row=0; row < matrixA[field].length; row++){
					System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] :");
					bufferedReader = new BufferedReader(new InputStreamReader(System.in));
					String dataMatrixA = null;
					try{
						dataMatrixA = bufferedReader.readLine();
					}catch(IOException error){
						System.out.println("Error! Input Data Correctly"+error.getMessage());
					}
					
					try{
						int DataMatrixA = Integer.parseInt(dataMatrixA);
						matrixA [field][row] = DataMatrixA;
					}
					catch(NumberFormatException e){
						System.out.println("Your Input is wrong");
					}
				}
			}
			
			System.out.println();
			System.out.println("Matriks A : "+ordo+" x " +ordo);
				for (int field=0; field<Ordo; field++){
					for(int row=0; row<Ordo; row++){
						System.out.print(matrixA[field][row]+ "  ");
					}
					System.out.println();
				}
				System.out.println();
				
		System.out.println("\nInput Data for Matriks B ");		
		System.out.println("OrdoB : "+ordo);
		
		int OrdoB = Integer.parseInt(ordo);
		int[][]matrixB = new int [OrdoB][OrdoB];
			
		System.out.println("Data for Matriks B :");
			for(int field=0; field<matrixB.length; field++){
				for (int row=0; row<matrixB[field].length; row++){
					System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] :");
					bufferedReader = new BufferedReader (new InputStreamReader (System.in));
					String dataMatrixB = null;
					
					try{
						dataMatrixB = bufferedReader.readLine();
					}
					catch (IOException error){
						System.out.println("Error! Input Data Correctly"+error.getMessage());
					}
					
						try{
							int DataMatrixB = Integer.parseInt(dataMatrixB);
							matrixB [field][row] = DataMatrixB;
						}
						catch (NumberFormatException e){
							System.out.println("Error! Input Data Correctly");	
						}
				}
			}
		
		System.out.println();
			System.out.println("\nMatrix B : "+ordo+" x " +ordo);
				for (int field=0; field < OrdoB; field++){
					for(int row=0; row < OrdoB; row++){
						System.out.print(matrixB[field][row]+ "  ");
					}
					System.out.println();
				}
				System.out.println();
		
		
		System.out.println("Result of the addition of the matrix A and matrix B is ");		
		int result [][] = new int [Ordo][Ordo];
		for (int field=0; field<Ordo ; field++) {
            for (int row=0; row<Ordo; row++) {
            	result [field][row] += matrixA[field][row] + matrixB[field][row];
            	System.out.print("   "+result[field][row]);
            }
            System.out.println();
		}
	}
}

class Reduction extends MatrixOOP{
	public static void reduction(){
		System.out.println("\nReduction Matrix");
		System.out.println("Input Data for Matriks A ");
		System.out.print("Ordo :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String ordo = null;
			try{
				ordo = bufferedReader.readLine();
			}
			catch(IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}
			
			
		int Ordo = Integer.parseInt(ordo);
		int[][]matrixA = new int [Ordo][Ordo];
		
		System.out.println("\nData for Matriks A");
			for (int field=0; field < matrixA.length; field++){
				for (int row=0; row < matrixA[field].length; row++){
					System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] :");
					bufferedReader = new BufferedReader(new InputStreamReader(System.in));
					String dataMatrixA = null;
					try{
						dataMatrixA = bufferedReader.readLine();
					}catch(IOException error){
						System.out.println("Error! Input Data Correctly"+error.getMessage());
					}
					
					try{
						int DataMatrixA = Integer.parseInt(dataMatrixA);
						matrixA [field][row] = DataMatrixA;
					}
					catch(NumberFormatException e){
						System.out.println("Your Input is wrong");
					}
				}
			}
			
			System.out.println();
			System.out.println("Matriks A : "+ordo+" x " +ordo);
				for (int field=0; field<Ordo; field++){
					for(int row=0; row<Ordo; row++){
						System.out.print(matrixA[field][row]+ "  ");
					}
					System.out.println();
				}
				System.out.println();
				
		System.out.println("\nInput Data for Matriks B ");		
		System.out.println("OrdoB : "+ordo);
		
		int OrdoB = Integer.parseInt(ordo);
		int[][]matrixB = new int [OrdoB][OrdoB];
			
		System.out.println("Data for Matriks B :");
			for(int field=0; field<matrixB.length; field++){
				for (int row=0; row<matrixB[field].length; row++){
					System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] :");
					bufferedReader = new BufferedReader (new InputStreamReader (System.in));
					String dataMatrixB = null;
					
					try{
						dataMatrixB = bufferedReader.readLine();
					}
					catch (IOException error){
						System.out.println("Error! Input Data Correctly"+error.getMessage());
					}
					
						try{
							int DataMatrixB = Integer.parseInt(dataMatrixB);
							matrixB [field][row] = DataMatrixB;
						}
						catch (NumberFormatException e){
							System.out.println("Error! Input Data Correctly");	
						}
				}
			}

		System.out.println();
			System.out.println("\nMatrix B : "+ordo+" x " +ordo);
				for (int field=0; field < OrdoB; field++){
					for(int row=0; row < OrdoB; row++){
						System.out.print(matrixB[field][row]+ "  ");
					}
					System.out.println();
				}
				System.out.println();
		
		
		System.out.println("Result of the reduction of the matrix A and matrix B is ");		
		int result [][] = new int [Ordo][Ordo];
		for (int field=0; field<Ordo ; field++) {
            for (int row=0; row<Ordo; row++) {
            	result [field][row] += matrixA[field][row] - matrixB[field][row];
            	System.out.print("   "+result[field][row]);
            }
            System.out.println();
		}	
	}
}

class Multiplication extends MatrixOOP{
	public static void multiplication (){
	
		System.out.println("Input Data for Matriks A ");
		System.out.print("Line :");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String lineA = null;
			try{
				lineA = bufferedReader.readLine();
			}catch(IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}
		
		System.out.print("Column :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String columnA = null;
		
			try{
				columnA = bufferedReader.readLine();
			}catch(IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}
			
			
			int LineA= Integer.parseInt(lineA);
			int ColumnA= Integer.parseInt(columnA);
			int [][]matrixA = new int [LineA][ColumnA];
			for (int field=0; field<matrixA.length; field++){
				for (int row=0; row<matrixA[field].length; row++){
					System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] :");
					bufferedReader = new BufferedReader(new InputStreamReader(System.in));
					String dataMatrixA =null;
					try{
						dataMatrixA = bufferedReader.readLine();
					}
					catch (IOException error){
						System.out.println("Error! Input Data Correctly"+error.getMessage());
					}
					
					try {
						int DataMatrixA = Integer.parseInt(dataMatrixA);
						matrixA [field][row] = DataMatrixA;
					}
					catch (NumberFormatException e){
						System.out.println("Your submit is wrong");
					}
			}
			}
		
			System.out.println();
			System.out.println("Matriks A : "+LineA+" x " +columnA);
				for (int field=0; field<LineA; field++){
					for(int row=0; row < ColumnA; row++){
						System.out.print(matrixA[field][row]+ " ");
					}
					System.out.println();
				}
				System.out.println();
		
	System.out.println("\nInput Data for Matriks B ");	
	System.out.println("Line B :"+columnA);
		System.out.print("Column B :");
		bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String columnB = null;
			try {
				columnB = bufferedReader.readLine();
			}catch(IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}

		
	int ColumnB= Integer.parseInt(columnB);
	int[][]matrixB = new int [ColumnA][ColumnB];
	for(int field=0; field < ColumnA; field++){
		for (int row=0; row < ColumnB; row++){
			System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] :");
			bufferedReader =  new BufferedReader (new InputStreamReader(System.in));
			String dataMatrixB =null;
			try{
				dataMatrixB = bufferedReader.readLine();
			}
			catch (IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}
			
			try {
				int DataMatrixB = Integer.parseInt(dataMatrixB);
				matrixB [field][row] = DataMatrixB;
			}
			catch (NumberFormatException e){
				System.out.println("Your submit is wrong");
			}
				}
			}
			
		
		
		System.out.println();
		System.out.println("\nMatrix B : "+columnA+" x " +columnB);
			for (int field=0; field < ColumnA; field++){
				for(int row=0; row < ColumnB; row++){
					System.out.print(matrixB[field][row]+ " ");
				}
				System.out.println();
			}
			System.out.println();
		
				
		int[][]matrixC = new int [ColumnA][ColumnB];
		System.out.println("result of the multiplication of the matrix A and matrix B is ");
			for(int field=0; field<LineA; field++){
				for(int row=0; row<ColumnB; row++){
					for (int field2=0; field2<ColumnA; field2++){
						matrixC[field][row] += matrixA[field][field2]*matrixB[field2][row];
					}
					System.out.print(matrixC[field][row]+" ");
				}
				System.out.println();
			}
	}
}

class Determinant extends MatrixOOP{
	public static void determinant (){
		System.out.println("Input Data Ordo 2x2 or 3x3");
		System.out.println("Ordo : ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String ordo = null;
			try{
				ordo = bufferedReader.readLine();
			}catch(IOException error){
				System.out.println("Error! Input Data Correctly"+error.getMessage());
			}
		
		int Ordo = Integer.parseInt(ordo);
		int [][]matrix = new int [Ordo][Ordo];
		if (Ordo == 2){
			for (int field=0; field<Ordo; field++){
				for (int row=0; row<Ordo; row++){
					System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] : ");
					bufferedReader = new BufferedReader(new InputStreamReader(System.in));
					String dataMatrix = null;
					try{
						dataMatrix = bufferedReader.readLine();
					}catch(IOException error){
						System.out.println("Error! Input Data Correctly"+error.getMessage());
					}
					
					try{
						int Matrix = Integer.parseInt(dataMatrix);
						matrix [field][row] = Matrix;
					}
					catch(NumberFormatException e){
						System.out.println("Your Input is wrong");
					}
				}
			}
			
			System.out.println();
			System.out.println("matrix : "+ordo+" X "+ordo);
			for (int field=0; field<Ordo; field++){
				for (int row=0; row<Ordo; row++){
					System.out.print("  "+matrix[field][row]+" ");
				}
				System.out.println();
			}
			System.out.println();
			int determinant = (matrix[0][0]*matrix[1][1])-(matrix[0][1]*matrix[1][0]);
					
					
			System.out.println("the results of the determinant of matrix "+ordo+" is  : "+determinant);

			
		}
		
		else if ( Ordo == 3){
		for (int field=0; field<Ordo; field++){
			for (int row=0; row<Ordo; row++){
				System.out.print("Line ["+(field+1)+"] Column ["+(row+1)+"] : ");
				bufferedReader = new BufferedReader(new InputStreamReader(System.in));
				String dataMatrix2 = null;
				try{
					dataMatrix2 = bufferedReader.readLine();
				}catch(IOException error){
					System.out.println("Error! Input Data Correctly"+error.getMessage());
				}
				
				try{
					int Matrix2 = Integer.parseInt(dataMatrix2);
					matrix [field][row] = Matrix2;
				}
				catch(NumberFormatException e){
					System.out.println("Your Input is wrong");
				}
				
			}
		}
		
		
		System.out.println();
		System.out.println("Matrix : "+ordo+" X "+ordo);
		for (int field=0; field<Ordo; field++){
			for (int row=0; row<Ordo; row++){
				System.out.print("  "+matrix [field][row]);	
			}
			System.out.println();
		}
		System.out.println();
			int determinant = matrix[0][0]*(matrix[1][1]*matrix[2][2]-matrix[1][2]*matrix[2][1])
							-matrix[0][1]*(matrix[1][0]*matrix[2][2]-matrix[1][2]*matrix[2][0])
							+matrix[0][2]*(matrix[1][0]*matrix[2][1]-matrix[1][1]*matrix[2][0]);	
		System.out.println("the results of the determinant is : "+determinant);
		}
		else{
			 System.out.println("Your input is wrong, please reinput your data, Thank you :)");
		}
	}
}

