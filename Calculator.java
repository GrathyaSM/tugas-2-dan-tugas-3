import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class Calculator {

		public static void main (String[] args){
			count basiccal = new count();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String input = null;
			int choice = 0;
			
			do{
				Menu();
				System.out.print("Submit your Choice : ");
				try{
					input = bufferedReader.readLine();
					try{
						choice = Integer.parseInt(input);
						if (choice>0 && choice==1 ){
							count.Addition();
						}else if (choice>0 && choice==2){
							count.Substraction();
						}else if (choice>0 && choice==3){
							count.divide();
						}else if (choice>0 && choice==4){
							count.Multiplication();
						}else{
							System.out.print("Thank You");
						}
					}catch (NumberFormatException e){
						System.out.println("Your Submit is wrong");
						}
				}catch (IOException error){
					System.out.print("Error Input" + error.getMessage());
					}
			}while (choice>0);
		}
		public static void Menu(){
			System.out.println("Simple Calculator");
			System.out.println("1. Addition");
			System.out.println("2. Substraction");
			System.out.println("3. Divide");
			System.out.println("4. Multiplication");
		}
}
class count extends Calculator{
	public static void Addition(){
		System.out.println("Addition of Number A and Number B");
		System.out.print("\nNumber A : ");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String NumberA = null;
		try{
			NumberA = bufferedReader.readLine();
		}catch (IOException error) {
			System.out.println("Error! input data correctly " + error.getMessage());
		}
		
		System.out.print("Number B : ");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String NumberB = null;
		try{
			NumberB = bufferedReader.readLine();
		}catch (IOException error){
			System.out.println ("Error! Input data correctly"+error.getMessage());
		}
		
		try{
			float numberA = Float.parseFloat (NumberA);
			float numberB = Float.parseFloat (NumberB);
			float result = numberA + numberB;
			System.out.println("Result of "+NumberA+" + "+NumberB+" = "+result);
		}catch (NumberFormatException e){
			System.out.println("Your Input is wrong");
		}	
	}
	
	public static void Substraction(){
		System.out.println("Substraction of Number A and Number B");
		System.out.print("NumberA : ");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String NumberA = null;
		try{
			NumberA = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data correctly"+error.getMessage());
		}
		
		System.out.print("NumberB : ");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String NumberB = null;
		try{
			NumberB = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data correctly"+error.getMessage());
		}
		
		try{
			float numberA = Float.parseFloat(NumberA);
			float numberB = Float.parseFloat(NumberB);
			float result = numberA-numberB;
			System.out.println("Result of "+numberA+" - "+numberB+" = "+result);
		}catch (NumberFormatException e){
			System.out.println("Your Input is wrong");
		}
	}
	
	public static void divide(){
		System.out.println("Divide of Number A and Number B");
		System.out.print("Number A : ");
		BufferedReader bufferedReader = new BufferedReader ( new InputStreamReader (System.in));
		String NumberA = null;
		try{
			NumberA = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! Input data correctly"+error.getMessage());
		}
		
		System.out.print("Number B : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String numberB = null;
		try{
			numberB = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! input data correctly"+error.getMessage());
		}
		
		try{
			float A = Float.parseFloat(NumberA);
            float B = Float.parseFloat(numberB);
            float result = A / B;
            System.out.println("Result of "+A+" / "+B+" : " + result);
		}catch(NumberFormatException error){
			System.out.println("Your input is wrong");
		}
	}
	
	public static void Multiplication(){
		System.out.println("Multiplication of Number A and Number B");
		System.out.print("Number A : ");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String NumberA = null;
		try{
			NumberA = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Error! input data correctly"+error.getMessage());
		}
		
		System.out.print("Number B : ");
		bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String NumberB = null;
		try{
			NumberB = bufferedReader.readLine();
		}catch(IOException error){
			System.out.println("Your input is wrong");
		}
		
		try{
			float numberA = Float.parseFloat(NumberA);
			float numberB = Float.parseFloat(NumberB);
			float result = numberA*numberB;
			System.out.println("Result of "+numberA+" x "+numberB+" = "+result);
		}catch(NumberFormatException error){
			System.out.println("Error! input data correctly"+error.getMessage());
		}
			
	}
}